#!/bin/bash

cd  $(echo $HOME)/csua

python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
python3 -m pip install typing-extensions

python3 client.py